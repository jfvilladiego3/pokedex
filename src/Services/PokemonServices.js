import axios from 'axios';

const API = 'https://pokeapi.co/api/v2/pokemon?limit=302'

/*GetUser */
export const getPokemons = () => {
  return  axios.get(`${API}`)
}

export const getPokemonsByURL = (URL) => {
  return  axios.get(`${URL}`)
}
