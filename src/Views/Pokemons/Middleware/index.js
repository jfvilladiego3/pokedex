import {
  GET_POKEMON_START,
  GET_POKEMON_ERROR,
  GET_POKEMON_SUCCESS
  } from '../../../Const/actionTypes'

import {getPokemons} from '../../../Services/PokemonServices'


export const applyMiddleware = dispatch => action => { 
 switch (action.type) { 
   case GET_POKEMON_START: 
   dispatch({ type: GET_POKEMON_START })
   
     return getPokemons().then(res => dispatch({  
           type: GET_POKEMON_SUCCESS, 
           payload: res.data.results })) 
       .catch(err => dispatch({ 
           type: GET_POKEMON_ERROR, 
           payload: err.response.data })) 
   default: dispatch(action) 
} }