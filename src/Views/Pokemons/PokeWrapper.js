import React, {useState, useEffect, useReducer, Suspense} from 'react';

/* Context */
import {PokeContext} from './PokeContext'
import { reducer, initialState } from './Reducers' 
import { useActions } from './Store' 
import { applyMiddleware } from './Middleware'
import { pokemonsListApi, isLoadingPokemons } from '../../Selectors'

/* Components */
import PokeList from './PokeList';
import Loanding from '../../Components/Loanding'

function PokeWrapper() {
  const [state, dispatch] = useReducer(reducer, initialState) 
  const actions = useActions(state, applyMiddleware(dispatch))
  
  useEffect(() => {
    actions.loadPokemons()
    console.log(state.loading);
  }, [])
  
  return (
    <PokeContext.Provider
    value={{ state, actions }}>
      <Suspense fallback={<Loanding/>}>
        {state.loading && <Loanding/> }
        {!state.loading && <PokeList/>}
      </Suspense>
    </PokeContext.Provider>
  )
}

export default PokeWrapper
