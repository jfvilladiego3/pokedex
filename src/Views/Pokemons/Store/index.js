import {
  GET_POKEMON_START,
  GET_POKEMON_ERROR,
  GET_POKEMON_SUCCESS
  } from '../../../Const/actionTypes'

export const useActions = (state, dispatch) => ({ 
  loadPokemons: data => { 
    dispatch({ type: GET_POKEMON_START }) 
    // dispatch({ type: GET_POKEMON_SUCCESS, payload: data }) 
  } 
})