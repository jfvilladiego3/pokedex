import React, {useContext, useEffect, useState, Suspense} from 'react';
import {PokeContext} from './PokeContext'

/*Components */
import Card from '../../Components/Cards/Cards'

/*Services */
import { getPokemonsByURL } from '../../Services/PokemonServices'
/*Compoenents */
import Loanding from '../../Components/Loanding'
/*Material-UI */
import {TextField} from '@material-ui/core';

export const urlImg = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"


function PokeList() {
  const [search, setSearch] = useState('')
  const { state } = useContext(PokeContext) 
  const { pokemons, loanding } = state

  useEffect(() => {
    console.log(state.loanding);
  }, [state])

  return (
    <Suspense fallback={<Loanding/>}>
        {loanding && <Loanding/> }
        <div className="poke_container poke_container__pokemons">
          {/* <TextField 
          name="search" 
          label="Search" 
          onChange={ (e) => setSearch(e.target.value) }
          /> */}
          {pokemons && 
          pokemons.filter( (pokemon, i) =>{
            if(pokemon.name.toLowerCase().indexOf(search.toLocaleLowerCase()) >= 0){

              return pokemon.name.toLowerCase().indexOf(search.toLocaleLowerCase()) >= 0
            }
          }).map( (pokemon, i) => 
            <Card key={i} name={pokemon.name} img={`${urlImg}${i+1}.png`} />  
          )}
        </div>
      </Suspense>
    
  )
}

export default PokeList
