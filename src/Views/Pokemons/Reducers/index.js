import {
  GET_POKEMON_START,
  GET_POKEMON_ERROR,
  GET_POKEMON_SUCCESS
  } from '../../../Const/actionTypes'

const initialState = { 
  pokemons: [],
  loading: false,
  error: ''
}

const reducer = (state = initialState, action) => { 
  
 switch (action.type) { 
   case GET_POKEMON_START: 
    return { ...state, loading: true} 
    break;
  case GET_POKEMON_ERROR: 
    return { ...state, loading: false, error: action.payload } 
    break;
  case GET_POKEMON_SUCCESS:
    return { ...state, loading: false, pokemons: action.payload } 
    break;
} } 
export { initialState, reducer }