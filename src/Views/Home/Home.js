import React from 'react'

import PokeWrapper from '../Pokemons/PokeWrapper'

function Home() {
  return (
    <div>
      <PokeWrapper/>
    </div>
  )
}

export default Home
