import React from 'react';
import logo from './logo.svg';
import './App.css';

import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './Views/Home/Home'
import Navbar from './Components/Navbar/Navbar'

function App(  ) {
  return (
      <Router>
        <div>
          <Navbar/>
          <Route exact path="/" component={Home} />
          {/* <Route exact path="/results/:movieName" component={Results} /> */}
        </div>
      </Router>
  );
}

export default App;
