import React from 'react'

import {
  AppBar,
  Toolbar,
  Typography
} from '@material-ui/core';

function Navbar() {
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">
            Pókedex
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Navbar
