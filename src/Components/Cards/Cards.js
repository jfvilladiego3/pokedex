import React, {Suspense} from 'react'

import Loanding from '../Loanding'

function Cards(props) {

  const {
    img,
    name
  } = props

  return (
    <Suspense fallback={<Loanding/>}>
      <div className="card fade-in">
        {img ?
          <img src={img} className='card__image fade-in' /> :
          <Loanding/>
        }
        <div className="card__name">
          {name}
        </div>
        <div className="card__content">
          
        </div>
      </div>
    </Suspense>

  )
}

export default Cards
