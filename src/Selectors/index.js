import { get } from 'lodash';

export const isLoadingPokemons = state => get(state, 'pokemons.isLoanding')
export const pokemonsListApi = state => get(state, 'pokemons.pokemons')